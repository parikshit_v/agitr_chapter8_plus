//This program spawns a new turtlesim turtle by calling
// the appropriate service.
#include <ros/ros.h>
//The srv class for the service.
#include <turtlesim/Spawn.h>
#include <geometry_msgs/Twist.h>  // For geometry_msgs::Twist
#include <iomanip> // for std::setprecision and std::fixed

geometry_msgs::Twist glob_msg;

// A callback function.  Executed each time a new pose
// message arrives.
void poseMessageReceived(const geometry_msgs::Twist& msg) {

  ROS_INFO_STREAM(std::setprecision(2) << std::fixed
    << "linear=(" <<  msg.linear.x << "," << " angular=" << msg.angular.z);
glob_msg.linear.x=msg.linear.x;
glob_msg.angular.z=msg.angular.z;
}






int main(int argc, char **argv){

    ros::init(argc, argv, "spawn_turtle");
    ros::NodeHandle nh;

//Create a client object for the spawn service. This
//needs to know the data type of the service and its name.
    ros::ServiceClient spawnClient
		= nh.serviceClient<turtlesim::Spawn>("spawn");

//Create the request and response objects.
    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x = 2;
    req.y = 3;
    req.theta = M_PI/2;
    req.name = "MyTurtle";

    ros::service::waitForService("spawn", ros::Duration(5));
    bool success = spawnClient.call(req,resp);

    if(success){
	ROS_INFO_STREAM("Spawned a turtle named "
			<< resp.name);
    }else{
	ROS_ERROR_STREAM("Failed to spawn.");
    }
     // Create a subscriber object.
  ros::Subscriber sub = nh.subscribe("turtle1/cmd_vel", 1000,
    &poseMessageReceived);

// Create a publisher object.
   ros::Publisher pub = nh.advertise<geometry_msgs::Twist>(
    "MyTurtle/cmd_vel", 1000);
geometry_msgs::Twist pubvel;
   // Loop at 2Hz until the node is shut down.
  ros::Rate rate(1);
  while(ros::ok()) {

    // Create and fill in the message.  The other four
    // fields, which are ignored by turtlesim, default to 0.
    
    pubvel.linear.x = glob_msg.linear.x;
    pubvel.angular.z = glob_msg.linear.z;

    // Publish the message.
    pub.publish(pubvel);

    // Send a message to rosout with the details.
    ROS_INFO_STREAM("Sending random velocity command:"
      << " linear=" << pubvel.linear.x
      << " angular=" << pubvel.angular.z);
  
       // Let ROS take over.
ros::spinOnce();
    // Wait until it's time for another iteration.
    rate.sleep();
}
}